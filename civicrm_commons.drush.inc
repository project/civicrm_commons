<?php
/**
 * @file
 * Provide (drush) command line access to synchronize users to contacts functionality.
 * This helps when you have a lot of contacts that cause timeouts or memory exhaustion.
 *
 * All Drupal code is released under the GNU General Public License.
 * Copyright (c) 2013 CivicActions, Inc.
 * Copyright (c) 2013 Fen Labalme
 */

/**
 * Implements hook_drush_help().
 */
function civicrm_commons_drush_help($command) {
  switch ($command) {
    case 'drush:civicrm-synchronize':
      return dt('Synchronize users from Drupal to CiviCRM. If no args, synchronize all.');
  }
}

/**
 * Implements hook_drush_command().
 */
function civicrm_commons_drush_command() {
  $items = array();
  $items['civicrm-synchronize'] = array(
    'description' => dt('Synchronize users from Drupal to CiviCRM. If no args, synchronize all.'),
    'arguments' => array(
      'offset' => '(optional) The offset of the first row to return',
      'count ' => '(optional) The maximum number of users to sync; no arg sets offset as count',
    ),
    'examples' => array(
      'Standard example' => 'drush civicrm-synchronize',
      'Argument example' => 'drush civicrm-synchronize 100 100',
    ),
    'aliases' => array('civisync'),
  );
  return $items;
}

/**
 * Callback function for drush civicrm-synchronize.
 */
function drush_civicrm_commons_civicrm_synchronize($offset = 0, $count = 0) {
  if (is_numeric($offset) && (intval($offset)==floatval($offset)) &&
      is_numeric($count)  && (intval($count) ==floatval($count))) {
    drush_log('Running civicrm-synchronize (this may take a while...)', 'ok');
    $status =_civicrm_commons_civicrm_synchronize(intval($offset), intval($count));
    drush_log($status, 'ok');
  }
  else {
    drush_die("civicrm-synchronize args (if included) must be integers.");
  }
}

/**
 * Adapted for use by drush from CRM_Core_BAO_CMSUser::synchronize()
 * Drupal only; removed cases for Joomla & Wordpress.
 * @param $max = The maximum number of users to sync; no arg or 0 means all.
 */
function _civicrm_commons_civicrm_synchronize($offset, $count) {
  civicrm_initialize();
  require_once 'CRM/Core/BAO/UFMatch.php';

  // get user records from Drupal users table
  $query = "SELECT uid, mail, name FROM {users} where mail != ''";
  if ($count) {
    $query .= " LIMIT $offset, $count";
  }
  elseif ($offset) {
    $query .= " LIMIT $offset";
  }
  $result = db_query($query);

  $user             = new StdClass();
  $contact_count    = 0;
  $contact_created  = 0;
  $contact_matching = 0;

  // synchronize users to contacts
  while ($row = $result->fetchAssoc()) {
    $user->uid  = $row['uid'];
    $user->mail = $row['mail'];
    $user->name = $row['name'];
    $contact_count++;
    if ($match = CRM_Core_BAO_UFMatch::synchronizeUFMatch($user, $row['uid'], $row['mail'], 'Drupal', 1, 'Individual', TRUE)) {
      $contact_created++;
    }
    else {
      $contact_matching++;
    }
    if (is_object($match)) {
      $match->free();
    }
  }
  // end of synchronization code

  $status = ts("Synchronize Users to Contacts completed.\n");
  $status .= ' ' . ts('Checked one user record.',
             array(
               'count' => $contact_count,
               'plural' => 'Checked %count user records.'
             )
  );
  if ($contact_matching) {
    $status .= ' ' . ts('Found one matching contact record.',
               array(
                 'count' => $contact_matching,
                 'plural' => 'Found %count matching contact records.'
               )
    );
  }

  $status .= ' ' . ts('Created one new contact record.',
             array(
               'count' => $contact_created,
               'plural' => 'Created %count new contact records.'
             )
  );
  return $status;
}
