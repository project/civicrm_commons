<?php
/**
 * @file
 * Sync first, last names and social media URLs from Drupal to CiviCRM during:
 *   + Drupal user -> CiviCRM contact synchronization
 *   + Drupal user account updates
 *
 * @todo Add bio field.
 * @todo Provide example api calls that provide access to CiviCRM fields from Drupal.
 *
 * All Drupal code is released under the GNU General Public License.
 * Copyright (c) 2013 CivicActions, Inc.
 * Copyright (c) 2013 Fen Labalme
 */

/**
 * Implements CiviCRM hook_civicrm_pre()
 * Generic hook checks iff appropriate function exists and calls it.
 */
function civicrm_commons_civicrm_pre($op, $object_name, $object_id, &$object_ref) {
  // watchdog('civicrm_commons',"hook_civicrm_pre_${op}_${objectName}(\$object_id, &\$object_ref)<pre>\n\$object_id=$object_id\n\$object_ref=" . var_export($object_ref, TRUE) . '</pre>');
  if (function_exists("civicrm_commons_civicrm_pre_${op}_$object_name")) {
    $tz = date_default_timezone_get();
    date_default_timezone_set('UTC');
    call_user_func("civicrm_commons_civicrm_pre_${op}_$object_name", $object_id, $object_ref);
    date_default_timezone_set($tz);
  }
}

/**
 * Implements CiviCRM hook_civicrm_pre() for op = create and objectName = UFMatch
 * This is invoked during initial user creation and during user->contact synchronization.
 */
function civicrm_commons_civicrm_pre_create_UFMatch($id, &$object_ref) {
  $cid = $object_ref['contact_id'];
  $uid = $object_ref['uf_id'];
  if (!$uid || !$cid) return;
  $user_obj = user_load($uid);
  // watchdog('civicrm_commons', 'pre_create_UFMatch:userObj=<pre>' . var_export($user_obj,TRUE) . '</pre>');
  _civicrm_commons_update('pre_UFMatch', $uid, $cid, $user_obj);
}

/**
 * Implements Drupal hook_user_update()
 * This is invoked when a user updates their profile.
 */
function civicrm_commons_user_update(&$edit, $account, $category) {
  civicrm_initialize();
  // watchdog('civicrm_commons','user_update:edit=<pre>' . var_export($edit, TRUE) . '</pre>');
  // watchdog('civicrm_commons','user_update:account=<pre>' . var_export($account, TRUE) . '</pre>');
  // watchdog('civicrm_commons','user_update:category=<pre>' . var_export($category, TRUE) . '</pre>');
  $uid = $account->uid;
  $params = array('version'    => 3,
                  'sequential' => 1,
                  'uf_id'      => $uid,
                  );
  $result = civicrm_api('UFMatch', 'Get', $params);
  if ($result['is_error']) {
    watchdog('civicrm_commons', "user_update: can't get contact_id for uid = @uid",
             array('@uid' => $uid), WATCHDOG_WARNING);
    return;
  }
  $cid = $result['values'][0]['contact_id'];
  if ($cid) {
    _civicrm_commons_update('user_update', $uid, $cid, $account);
  }
}

/**
 * Sync first, last name and social media URL fields from Commons to CiviCRM.
 * Note that syncing is one way: fields changed in CiviCRM do not sync back to Commons.
 */
function _civicrm_commons_update($op, $uid, $cid, $user_obj) {
  $params = array(
                  'version'      => 3,
                  'contact_type' => 'Individual',
                  'contact_id'   => $cid,
                  );
  $data = 0;

  // Get first and last names
  $first_name = trim($user_obj->field_name_first['und'][0]['value']);
  $last_name  = trim($user_obj->field_name_last['und'][0]['value']);
  if ($first_name) {
    $params['first_name'] = $first_name;
    $data++;
  }
  if ($last_name) {
    $params['last_name'] = $last_name;
    $data++;
  }

  // Get Social Media URLs
  // $urls = array('field_facebook_url' => 'Facebook URL',
  //               'field_linkedin_url' => 'LinkedIn URL',
  //               'field_twitter_url'  => 'Twitter URL');
  // FIXME: hard-coded contact custom id values suck (but they're fast!)
  // http://wiki.civicrm.org/confluence/display/CRMDOC42/Setting+and+getting+custom+field+values+from+within+hooks
  $urls = array(1 => 'facebook', 2 => 'linkedin', 7 => 'twitter');
  foreach ($urls as $id => $name) {
    $field = 'field_' . $name . '_url';
    $value = trim($user_obj->{$field}['und'][0]['url']);
    if ($value) {
      // Simple-minded URI cleanup; consider using HTMLPurifier_AttrDef_URI:validate()
      if (strncasecmp($value, 'http', 4)) {
        $value = 'http://' . $value;
      }
      // $customFieldID = CRM_Core_BAO_CustomField::getCustomFieldID($label, 'Social Media');
      $params['custom_' . $id] = $value;
      $data++;
    }
  }

  // Add client-specific contact record params
  if (function_exists("civicrm_add_contact_params")) {
    civicrm_add_contact_params($uid, $data, $params);
  }

  if ($data) {
    $result = civicrm_api('Contact', 'create', $params);
    $status = $result['is_error'] ? WATCHDOG_ERROR : WATCHDOG_NOTICE;
    watchdog('civicrm_commons', $op . ': uid=@uid, cid=@cid, params=<pre>@params</pre>',
             array('@uid'    => $uid,
                   '@cid'    => $cid,
                   '@params' => print_r($params, TRUE),
                   ), $status);
  }
}
